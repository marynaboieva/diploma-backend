import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Unique,
  OneToMany,
  BeforeInsert,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ChatMembership } from 'src/chats/chat-membership.entity';
import { ChatInvite } from '../chat-invites/chat-invite.enity';
import { Message } from '../messages/message.entity';
import { MessageStatus } from '../messages/message-status.entity';
import { Friendship } from '../friendship/friendship.entity';

@Entity()
@Unique(['email'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }

  @OneToMany(() => Friendship, (friendRequest) => friendRequest.sender)
  sentFriendshipRequests: Friendship[];

  @OneToMany(() => Friendship, (friendRequest) => friendRequest.receiver)
  receivedFriendshipRequests: Friendship[];

  @OneToMany(() => ChatMembership, chatMembership => chatMembership.chat)
  chats: ChatMembership[];

  @OneToMany(() => ChatInvite, invite => invite.createdBy)
  createdInvites: ChatInvite[];

  @OneToMany(() => MessageStatus, msgStatus => msgStatus.receiver)
  receivedMessages: MessageStatus[];

  @OneToMany(() => Message, message => message.sender)
  sentMessages: Message[];

}
