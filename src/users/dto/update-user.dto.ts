import { IsOptional, IsNotEmpty } from 'class-validator';

export class UpdateUserDto {
    @IsOptional()
    @IsNotEmpty()
    password: string;

    @IsOptional()
    @IsNotEmpty()
    username: string;
}
