import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';

export class SearchUserDto {
    @IsNotEmpty()
    username: string;

    @IsOptional()
    @Type(() => Number)
    @IsInt()
    page?: number;


    @IsOptional()
    @Type(() => Number)
    @IsInt()
    limit?: number;
}
