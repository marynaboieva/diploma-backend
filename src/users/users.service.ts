import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Friendship } from 'src/friendship/friendship.entity';
import { Repository } from 'typeorm';

import { CreateUserDto } from './dto/create-user.dto';
import { SearchUserDto } from './dto/search-users.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) { }

  async create(createUserDto: CreateUserDto): Promise<Partial<User>> {
    const user = new User();

    user.email = createUserDto.email;
    user.password = createUserDto.password;
    user.username = createUserDto.username;
    const { id } = await this.usersRepository.save(user);
    return { id, username: user.username, email: user.email };
  }

  findById(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  getUserProfile(id: number): Promise<User> {
    return this.usersRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id })
      .select(['user.id', 'user.username', 'user.email'])
      .getOne();
  }

  findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({ where: { email } });
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }

  update(id: number, userData: UpdateUserDto) {
    return this.usersRepository.update({ id }, userData);
  }

  checkIfExists({ email, username }: CreateUserDto): Promise<User> {
    return this.usersRepository
      .createQueryBuilder('user')
      .where('email = :email OR username = :username', {
        email,
        username,
      })
      .getOne();
  }

  async searchByUsername(
    userId: User['id'],
    { limit = 10, page = 0, username }: SearchUserDto,
  ) {
    const [users, total] = await this.usersRepository
      .createQueryBuilder('user')
      .where('user.username LIKE :username', { username: `%${username}%` })
      .andWhere('user.id != :userId', { userId })
      .leftJoinAndMapOne(
        'user.friendship',
        Friendship,
        'friendship',
        'friendship.receiverId = :userId AND friendship.senderId=user.id OR friendship.senderId = :userId AND friendship.receiverId=user.id',
        { userId },
      )

      .select([
        'user.id',
        'user.username',
        'friendship.status',
        'friendship.senderId',
        'friendship.receiverId',
      ])
      .orderBy('user.id', 'DESC')
      .skip(page * limit)
      .take(limit)
      .getManyAndCount();

    return { users, total };
  }
}
