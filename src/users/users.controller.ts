import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { JWTPayload } from 'src/decorators/jwt-payload.decorator';
import { Public } from 'src/decorators/public.decorator';
import { CreateUserDto } from './dto/create-user.dto';
import { SearchUserDto } from './dto/search-users.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Public()
  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<Partial<User>> {
    if (await this.usersService.checkIfExists(createUserDto)) {
      throw new BadRequestException(
        `User with the provided email or username already exists`,
      );
    }
    return await this.usersService.create(createUserDto);
  }

  @Get()
  async findOne(@JWTPayload() jwtPayload): Promise<Partial<User>> {
    return await this.usersService.getUserProfile(jwtPayload.id);
  }

  @Delete()
  async remove(@JWTPayload() jwtPayload): Promise<void> {
    return await this.usersService.remove(jwtPayload.id);
  }

  @Put()
  async update(@JWTPayload() jwtPayload, @Body() updateUserDto: UpdateUserDto) {
    return await this.usersService.update(jwtPayload.id, updateUserDto);
  }

  @Get('/search')
  async searchUsers(
    @Query() { username, page, limit }: SearchUserDto,
    @JWTPayload() jwtPayload,
  ) {
    return await this.usersService.searchByUsername(jwtPayload.id, {
      page,
      username,
      limit,
    });
  }
}
