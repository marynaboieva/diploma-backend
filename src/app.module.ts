import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { TypeOrmModule } from '@nestjs/typeorm';

// Modules
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { FriendshipModule } from './friendship/friendship.module';
import { ChatsModule } from './chats/chats.module';
import { ChatInvitesModule } from './chat-invites/chat-invites.module';
import { MessagesModule } from './messages/messages.module';

// Entities
import { User } from './users/user.entity';
import { Friendship } from './friendship/friendship.entity';
import { Chat } from './chats/chat.entity';
import { ChatMembership } from './chats/chat-membership.entity';
import { ChatInvite } from './chat-invites/chat-invite.enity';
import { Message } from './messages/message.entity';
import { MessageStatus } from './messages/message-status.entity';

const entities = [
  User,
  Friendship,
  Chat,
  ChatMembership,
  ChatInvite,
  Message,
  MessageStatus,
];

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: Number.parseInt(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      synchronize: true,
      autoLoadEntities: true,
      // logging: true,
      entities,
    }),
    UsersModule,
    AuthModule,
    FriendshipModule,
    ChatsModule,
    ChatInvitesModule,
    MessagesModule,
  ],
})
export class AppModule { }
