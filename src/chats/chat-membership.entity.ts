import { User } from 'src/users/user.entity';
import {
    Entity,
    Column,
    CreateDateColumn,
    ManyToOne,
    PrimaryColumn,
} from 'typeorm';
import { Chat } from './chat.entity';
import { ChatRole } from './types';


@Entity()
export class ChatMembership {

    @PrimaryColumn()
    chatId: number;

    @ManyToOne(() => Chat, chat => chat.members, { onDelete: 'CASCADE' })
    chat: Chat;

    @PrimaryColumn()
    userId: number;

    @ManyToOne(() => User, user => user.chats, { onDelete: 'CASCADE' })
    user: User;

    @Column({
        type: 'enum',
        enum: ChatRole,
        default: ChatRole.MEMBER,
    })
    role: ChatRole;

    @CreateDateColumn({ type: 'timestamp' })
    joinedAt: Date;
}
