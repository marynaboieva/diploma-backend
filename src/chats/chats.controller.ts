import {
    BadRequestException, Body, Controller,
    Delete, ForbiddenException, Get, NotFoundException,
    Param, ParseIntPipe, Post, Put, Query
} from '@nestjs/common';
import { JWTPayload } from 'src/decorators/jwt-payload.decorator';
import { UsersService } from 'src/users/users.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { CreateInviteDto } from '../chat-invites/dto/create-invite.dto';
import { UpdateMemberDto, } from './dto/update-member.dto';
import { UpdateChatDto } from './dto/update.chat.dto';
import { Chat } from './chat.entity';
import { ChatsService } from './chats.service';
import { ChatRole } from './types';
import { MessagesService } from '../messages/message.service';
import { ChatInvitesService } from '../chat-invites/chat-invites.service';
import { GetMessagesDto } from 'src/messages/dto/get-messages.dto';
import { CreateMessageDto } from 'src/messages/dto/create-message.dto';

@Controller('chats')
export class ChatsController {
    constructor(
        private readonly chatsService: ChatsService,
        private readonly usersService: UsersService,
        private readonly chatInvitesService: ChatInvitesService,
        private readonly messagesService: MessagesService
    ) { }

    @Get()
    async getUserChats(@JWTPayload() jwtPayload,) {
        return await this.chatsService.getUserChats(jwtPayload.id);
    }

    @Post()
    async create(
        @Body() createChatDto: CreateChatDto,
        @JWTPayload() jwtPayload,
    ): Promise<Partial<Chat>> {
        const chat = await this.chatsService.create(createChatDto);
        const membership = await this.chatsService.addMember(chat.id, jwtPayload.id, ChatRole.ADMIN)
        return chat;
    }

    @Delete(':chatId')
    async deleteChat(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
    ) {
        const chat = await this.chatsService.findById(chatId);
        if (!chat) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }
        return await this.chatsService.delete(chatId);
    }

    @Put(':chatId')
    async updateChat(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Body() updateChatDto: UpdateChatDto,
    ) {
        const chat = await this.chatsService.findById(chatId);
        if (!chat) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }
        return await this.chatsService.update(chatId, updateChatDto);
    }

    @Get(':chatId/members')
    async getChatWithMembers(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (!chatUser) {
            throw new ForbiddenException();
        }

        return await this.chatsService.getChatWithMembers(chatId);
    }

    @Post(':chatId/members/:userId')
    async addChatMember(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Param('userId', ParseIntPipe) userId: number,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }

        if (!await this.usersService.findById(userId)) {
            throw new NotFoundException('User was not found');
        }

        if (await this.chatsService.getUserInChat(chatId, userId)) {
            throw new BadRequestException('This user is already added to the chat');
        }

        return await this.chatsService.addMember(chatId, userId);
    }

    @Delete(':chatId/members/:userId')
    async deleteChatMember(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Param('userId', ParseIntPipe) userId: number,
    ) {
        if (userId === jwtPayload.id) {
            throw new BadRequestException();
        }

        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }

        if (!await this.usersService.findById(userId)) {
            throw new NotFoundException('User was not found');
        }

        if (!await this.chatsService.getUserInChat(chatId, userId)) {
            throw new NotFoundException('User is not a member of the chat');
        }

        return await this.chatsService.deleteMember(chatId, userId);
    }

    @Put(':chatId/members/:userId')
    async updateChatMember(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Param('userId', ParseIntPipe) userId: number,
        @Body() updateMemberDto: UpdateMemberDto,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }

        if (!await this.usersService.findById(userId)) {
            throw new NotFoundException('User was not found');
        }

        if (!await this.chatsService.getUserInChat(chatId, userId)) {
            throw new NotFoundException('User is not a member of the chat');
        }

        return await this.chatsService.updateMember(chatId, userId, updateMemberDto);
    }

    @Post(':chatId/leave')
    async leaveChat(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        if (!(await this.chatsService.getUserInChat(chatId, jwtPayload.id))) {
            throw new NotFoundException('User is not a member of the chat');
        }

        return await this.chatsService.deleteMember(chatId, jwtPayload.id);
    }


    @Post(':chatId/invites')
    async createInvite(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Body() { expiresAt }: CreateInviteDto
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }

        return await this.chatInvitesService.createInvite(chatId, jwtPayload.id, expiresAt);
    }

    @Get(':chatId/messages/')
    async getChatMessages(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Query() { page, limit }: GetMessagesDto,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (!chatUser) {
            throw new ForbiddenException("You are not a member of the chat");
        }

        return this.messagesService.getMessagesByChat(chatId, jwtPayload.id, { page, limit });
    }

    @Post(':chatId/messages/')
    async createChatMessage(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Body() createMessageDto: CreateMessageDto,
    ) {
        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (!chatUser) {
            throw new ForbiddenException("You are not a member of the chat");
        }

        return this.messagesService.create(createMessageDto, jwtPayload.id, chatId);
    }
}
