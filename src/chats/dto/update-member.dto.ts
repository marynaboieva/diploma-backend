import { IsEnum } from 'class-validator';
import { ChatRole } from '../types';

export class UpdateMemberDto {
    @IsEnum(ChatRole)
    role: ChatRole;
}
