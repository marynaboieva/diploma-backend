import { forwardRef, Module } from '@nestjs/common';
import { ChatsController } from './chats.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsService } from './chats.service';
import { Chat } from './chat.entity';
import { ChatMembership } from './chat-membership.entity';

import { UsersModule } from '../users/users.module';
import { MessagesModule } from 'src/messages/messages.module';
import { ChatInvitesModule } from 'src/chat-invites/chat-invites.module';


@Module({
  imports: [
    TypeOrmModule.forFeature([Chat, ChatMembership]),
    forwardRef(() => ChatInvitesModule),
    UsersModule, forwardRef(() => MessagesModule)
  ],
  providers: [ChatsService],
  controllers: [ChatsController],
  exports: [ChatsService],
})
export class ChatsModule { }
