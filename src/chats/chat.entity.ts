import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    OneToMany,
} from 'typeorm';
import { ChatInvite } from '../chat-invites/chat-invite.enity';
import { ChatMembership } from './chat-membership.entity';
import { Message } from '../messages/message.entity';


@Entity()
export class Chat {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @OneToMany(() => ChatMembership, chatMembership => chatMembership.chat)
    members: ChatMembership[];

    @OneToMany(() => ChatInvite, invite => invite.chat)
    invites: ChatInvite[];

    @OneToMany(() => Message, (msgToChat) => msgToChat.chat)
    messages: Message[];
}
