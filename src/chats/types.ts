export enum ChatRole {
    ADMIN = 'ADMIN',
    MEMBER = 'MEMBER'
}
