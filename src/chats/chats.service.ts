import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/user.entity';
import { Repository } from 'typeorm';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { UpdateChatDto } from './dto/update.chat.dto';
import { ChatInvite } from '../chat-invites/chat-invite.enity';
import { ChatMembership } from './chat-membership.entity';
import { Chat } from './chat.entity';
import { ChatRole } from './types';

@Injectable()
export class ChatsService {
    constructor(
        @InjectRepository(Chat)
        private readonly chatsRepository: Repository<Chat>,
        @InjectRepository(ChatMembership)
        private readonly chatMembershipRepository: Repository<ChatMembership>,
    ) { }

    async create(createChatDto: CreateChatDto): Promise<Partial<Chat>> {
        const chat = new Chat();
        chat.name = createChatDto.name;
        const { id } = await this.chatsRepository.save(chat);


        return { id, name: chat.name };
    }

    delete(id: Chat["id"]) {
        return this.chatsRepository.delete(id);
    }

    update(id: Chat["id"], chatData: UpdateChatDto) {
        return this.chatsRepository.update({ id }, chatData);
    }

    findById(id: number): Promise<Chat> {
        return this.chatsRepository.findOne(id);
    }

    getUserInChat(chatId: Chat["id"], userId: User['id']) {
        return this.chatMembershipRepository.findOne({ where: { chatId, userId } });
    }

    getUserChats(userId: User["id"]) {
        return this.chatsRepository
            .createQueryBuilder('chat')
            .leftJoinAndSelect('chat.members', 'member')
            .select(['chat.id', 'chat.createdAt', 'chat.name'
            ])
            .where("member.userId = :userId", { userId })
            .getMany()
    }

    getChatWithMembers(chatId: Chat["id"]) {
        return this.chatsRepository
            .createQueryBuilder('chat')
            .leftJoinAndSelect('chat.members', 'member')
            .leftJoinAndSelect('member.user', 'user')
            .select(['chat.id', 'chat.name', 'chat.createdAt',
                'member.joinedAt', 'member.role',
                'user.id', 'user.username'
            ])
            .where("chat.id = :chatId", { chatId })
            .getOne()
    }

    addMember(chatId: Chat["id"], userId: User['id'], role: ChatRole = ChatRole.MEMBER) {
        const membership = new ChatMembership();
        membership.chatId = chatId;
        membership.userId = userId;
        membership.role = role;

        return this.chatMembershipRepository.save(membership);
    }

    deleteMember(chatId: Chat["id"], userId: User['id']) {
        return this.chatMembershipRepository.delete({ chatId, userId });
    }

    updateMember(chatId: Chat["id"], userId: User['id'], memberData: UpdateMemberDto) {
        return this.chatMembershipRepository.createQueryBuilder()
            .update(ChatMembership)
            .set(memberData)
            .where("chatId = :chatId AND userId = :userId", { chatId, userId })
            .execute();
    }

}
