import { User } from 'src/users/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  PrimaryColumn,
} from 'typeorm';

// Types
import { FriendshipStatus } from './types';

@Entity()
export class Friendship {
  @ManyToOne(() => User, (user) => user.sentFriendshipRequests)
  @JoinColumn({ name: "senderId" })
  sender: User;

  @PrimaryColumn({ nullable: false })
  senderId: number;

  @ManyToOne(() => User, (user) => user.receivedFriendshipRequests)
  @JoinColumn({ name: "receiverId" })
  receiver: User;

  @PrimaryColumn({ nullable: false })
  receiverId: number;

  @Column({
    type: 'enum',
    enum: FriendshipStatus,
    default: FriendshipStatus.PENDING,
  })
  status: FriendshipStatus;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
