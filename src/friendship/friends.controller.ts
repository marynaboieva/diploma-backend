import {
  Controller,
  Post,
  Get,
  Param,
  Put,
  Body,
  ParseIntPipe,
  NotFoundException,
  ForbiddenException,
  Delete,
  BadRequestException,
} from '@nestjs/common';
import { FriendshipsService } from './friendship.service';

import { JWTPayload } from 'src/decorators/jwt-payload.decorator';
import { UpdateStatusDTO } from './dto/update-status.dto';
import { FriendshipStatus } from './types';
import { UsersService } from 'src/users/users.service';

@Controller('friends')
export class FriendshipsController {
  constructor(
    private readonly friendshipService: FriendshipsService,
    private readonly usersService: UsersService,
  ) { }

  @Get('')
  async getUsersFriends(@JWTPayload() jwtPayload) {
    return await this.friendshipService.getFriends(jwtPayload.id);
  }

  @Post(':receiverId')
  async addFriend(
    @Param('receiverId', ParseIntPipe) receiverId: number,
    @JWTPayload() jwtPayload,
  ) {
    if (receiverId === jwtPayload.id) {
      throw new BadRequestException('You can not add yourself as a friend :)');
    }
    const receiver = await this.usersService.findById(receiverId);
    if (!receiver) {
      throw new NotFoundException('User was not found');
    }
    return await this.friendshipService.addFriend(receiverId, jwtPayload.id);
  }

  @Delete(':receiverId')
  async removeFriend(
    @Param('receiverId', ParseIntPipe) receiverId: number,
    @JWTPayload() jwtPayload,
  ) {
    const currentStatus = await this.friendshipService.getFriendshipStatus(
      receiverId,
      jwtPayload.id,
    );

    if (
      !currentStatus ||
      (currentStatus.status !== FriendshipStatus.ACCEPTED &&
        jwtPayload.id !== currentStatus.senderId)
    ) {
      throw new NotFoundException('The user is not in your friends list');
    }

    await this.friendshipService.removeFriend(receiverId, jwtPayload.id);
    return;
  }

  @Put(':senderId/status')
  async updateFriendshipStatus(
    @Param('senderId', ParseIntPipe) senderId: number,
    @Body() { status }: UpdateStatusDTO,
    @JWTPayload() jwtPayload,
  ) {
    const currentStatus = await this.friendshipService.getFriendshipStatus(
      senderId,
      jwtPayload.id,
    );

    if (!currentStatus) {
      throw new NotFoundException(
        "You don't have a received friendship request from this user",
      );
    }

    if (currentStatus.senderId === jwtPayload.id) {
      throw new ForbiddenException("You aren't a receiver of the request");
    }

    return await this.friendshipService.updateFriendshipStatus(
      senderId,
      jwtPayload.id,
      status,
    );
  }
}
