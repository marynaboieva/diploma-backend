import { IsEnum, NotEquals } from 'class-validator';
import { FriendshipStatus } from "../types";

export class UpdateStatusDTO {
    @IsEnum(FriendshipStatus)
    @NotEquals(FriendshipStatus[FriendshipStatus.PENDING])
    status: FriendshipStatus;
}