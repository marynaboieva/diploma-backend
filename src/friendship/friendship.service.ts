import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Friendship } from './friendship.entity';
import { FriendshipStatus } from './types';

@Injectable()
export class FriendshipsService {
  constructor(
    @InjectRepository(Friendship)
    private readonly friendshipRepository: Repository<Friendship>,
  ) { }

  async addFriend(receiverId: number, senderId: number) {
    const existingRequest = await this.getFriendshipStatus(
      receiverId,
      senderId,
    );
    if (existingRequest) {
      return existingRequest;
    }

    const friendship = {
      senderId,
      receiverId,
      status: FriendshipStatus.PENDING,
    };
    return this.friendshipRepository.save(friendship);
  }

  async getFriendshipStatus(receiverId: number, senderId: number) {
    return this.friendshipRepository.findOne({
      where: [
        { senderId, receiverId },
        { senderId: receiverId, receiverId: senderId },
      ],
    });
  }

  removeFriend(receiverId: number, senderId: number) {

    return this.friendshipRepository.createQueryBuilder()
      .delete()
      .where("receiverId = :receiverId AND senderId = :senderId",
        { receiverId, senderId }
      )
      .orWhere("receiverId = :senderId AND senderId = :receiverId",
        { receiverId, senderId }
      )
      .execute();
  }

  updateFriendshipStatus(
    receiverId: number,
    senderId: number,
    status: FriendshipStatus,
  ) {
    return this.friendshipRepository
      .createQueryBuilder()
      .update(Friendship)
      .set({ status })
      .where("receiverId = :receiverId AND senderId = :senderId",
        { receiverId, senderId }
      )
      .orWhere("receiverId = :senderId AND senderId = :receiverId",
        { receiverId, senderId }
      )
      .execute();
  }

  getFriends(userId: number) {
    return this.friendshipRepository
      .createQueryBuilder('friendship')
      .where({ senderId: userId })
      .orWhere({ receiverId: userId })
      .leftJoinAndSelect('friendship.sender', 'sender')
      .leftJoinAndSelect('friendship.receiver', 'receiver')
      .select([
        'friendship.status',
        'friendship.updatedAt',
        'friendship.createdAt',
        'sender.id',
        'sender.username',
        'receiver.id',
        'receiver.username',
      ])
      .getMany();
  }
}
