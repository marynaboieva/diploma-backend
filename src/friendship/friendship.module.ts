import { Module } from '@nestjs/common';
import { FriendshipsService } from './friendship.service';
import { FriendshipsController } from './friends.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Friendship } from './friendship.entity';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([Friendship]), UsersModule],
  providers: [FriendshipsService],
  controllers: [FriendshipsController],
})
export class FriendshipModule { }
