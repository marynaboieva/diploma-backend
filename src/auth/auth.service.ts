import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserJWTPayload } from './types';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService, private jwtService: JwtService
  ) { }

  async validateUser(email: string, pass: string): Promise<UserJWTPayload | null> {
    const user = await this.usersService.findByEmail(email);

    if (!(await user?.validatePassword(pass))) {
      return null;
    }

    return { id: user.id, email: user.email, username: user.username };
  }

  async login(payload: UserJWTPayload) {
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}