export type UserJWTPayload = {
    id: number;
    username: string;
    email: string;
}