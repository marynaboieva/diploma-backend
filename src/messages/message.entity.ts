import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    OneToOne,
} from 'typeorm';
import { MessageStatus } from './message-status.entity';
import { User } from '../users/user.entity';
import { Chat } from '../chats/chat.entity';


@Entity()
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    body: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @ManyToOne(() => User, (user) => user.sentMessages)
    @JoinColumn({ name: "senderId" })
    sender: User;

    @Column()
    senderId: number;

    @ManyToOne(() => Chat, chat => chat.messages, { onDelete: 'CASCADE' })
    @JoinColumn({ name: "chatId" })
    chat: Chat;

    @Column()
    chatId: number;

    @OneToMany(() => MessageStatus, (status) => status.message)
    status: MessageStatus[];

    @OneToOne(() => Message, message => message.id)
    @JoinColumn({ name: "replyToId" })
    replyTo: Message;

    @Column({ default: null, nullable: true })
    replyToId: number;
}
