import {
    Entity,
    ManyToOne,
    JoinColumn,
    CreateDateColumn,
    PrimaryColumn,
} from 'typeorm';
import { Message } from './message.entity';
import { User } from '../users/user.entity';

// ????????????
@Entity()
export class MessageStatus {

    @ManyToOne(() => User, (user) => user.receivedMessages)
    @JoinColumn({ name: "receiverId" })
    receiver: User;

    @PrimaryColumn()
    receiverId: number;

    @ManyToOne(() => Message, (message) => message.status)
    @JoinColumn({ name: "messageId" })
    message: Message;

    @PrimaryColumn()
    messageId: number;

    @CreateDateColumn({ type: 'timestamp' })
    readAt: Date;
}
