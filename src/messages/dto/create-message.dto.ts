import { IsNotEmpty, IsString, IsNumber, IsOptional } from 'class-validator';

export class CreateMessageDto {
    @IsString()
    @IsNotEmpty()
    body: string;

    @IsNumber()
    @IsOptional()
    replyToId: number;
}
