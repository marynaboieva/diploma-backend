import { IsNotEmpty, IsString, IsNumber, IsOptional } from 'class-validator';

export class UpdateMessageDto {
    @IsString()
    @IsNotEmpty()
    body: string;

    @IsNumber()
    @IsOptional()
    replyToId: number;
}
