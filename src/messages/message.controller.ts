import {
    BadRequestException,
    Body, Controller,
    Delete, ForbiddenException, Get, NotFoundException,
    Param, ParseIntPipe, Put
} from '@nestjs/common';
import { ChatsService } from 'src/chats/chats.service';
import { ChatRole } from 'src/chats/types';
import { JWTPayload } from 'src/decorators/jwt-payload.decorator';
import { UpdateMessageStatusDto } from './dto/update-message-status.dto';

import { UpdateMessageDto } from './dto/update-message.dto';
import { MessagesService } from './message.service';

@Controller('messages')
export class MessagesController {
    constructor(
        private readonly messagesService: MessagesService,
        private readonly chatsService: ChatsService,

    ) { }

    @Get('/unread')
    async getUnreadMessages(
        @JWTPayload() jwtPayload,
    ) {
        return await this.messagesService.getUnreadMessages(jwtPayload.id);
    }

    @Put('/:msgId/status')
    async updateReadStatus(
        @JWTPayload() jwtPayload,
        @Param('msgId', ParseIntPipe) msgId: number,
        @Body() { read }: UpdateMessageStatusDto,
    ) {
        const message = await this.messagesService.findById(msgId);
        if (!message) {
            throw new NotFoundException("Message was not found");
        }

        if (message.senderId === jwtPayload.id) {
            throw new BadRequestException("Can not change your own message read status");
        }

        const chatUser = await this.chatsService
            .getUserInChat(message.chatId, jwtPayload.id);
        if (!chatUser) {
            throw new ForbiddenException("You are not a member of this chat");
        }

        return await this.messagesService.updateStatus(msgId, jwtPayload.id, read);
    }

    @Delete('/:msgId')
    async deleteMessage(
        @JWTPayload() jwtPayload,
        @Param('msgId', ParseIntPipe) msgId: number,
    ) {
        const message = await this.messagesService.findById(msgId);
        if (!message) {
            throw new NotFoundException("Message was not found");
        }

        const chatUser = await this.chatsService
            .getUserInChat(message.chatId, jwtPayload.id);
        if (message.senderId !== jwtPayload.id || chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }
        return await this.messagesService.delete(msgId);
    }

    @Put(':msgId')
    async updateMessage(
        @JWTPayload() jwtPayload,
        @Param('msgId', ParseIntPipe) msgId: number,
        @Body() updateMessageDto: UpdateMessageDto,
    ) {
        const message = await this.messagesService.findById(msgId);
        if (!message) {
            throw new NotFoundException("Message was not found");
        }

        if (message.senderId !== jwtPayload.id) {
            throw new ForbiddenException("You do not have permission for this action");
        }
        return await this.messagesService.update(msgId, updateMessageDto);
    }
}
