import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Chat } from '../chats/chat.entity';
import { User } from '../users/user.entity';
import { Message } from './message.entity';

import { CreateMessageDto } from './dto/create-message.dto';
import { GetMessagesDto } from './dto/get-messages.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { ChatsService } from 'src/chats/chats.service';
import { MessageStatus } from './message-status.entity';

@Injectable()
export class MessagesService {
    constructor(
        @InjectRepository(Message)
        private readonly messagesRepository: Repository<Message>,

        @InjectRepository(MessageStatus)
        private readonly messageStatusRepository: Repository<MessageStatus>,

        private readonly chatsService: ChatsService,
    ) { }

    async create(
        createMessageDto: CreateMessageDto,
        userId: User['id'], chatId: Chat['id']
    ): Promise<Message> {
        const message = new Message();
        message.body = createMessageDto.body;
        message.senderId = userId;
        message.chatId = chatId;

        if (createMessageDto.replyToId) {
            const replyToMessage = await this.findById(createMessageDto.replyToId);
            message.replyToId = replyToMessage?.chatId === chatId ? replyToMessage.id : null;
        }

        return this.messagesRepository.save(message);
    }

    delete(id: Message["id"]) {
        return this.messagesRepository.delete(id);
    }

    update(id: Message["id"], messageData: UpdateMessageDto) {
        return this.messagesRepository.update({ id }, messageData);
    }

    async updateStatus(id: Message["id"], userId: User['id'], isRead: boolean) {
        const status = await this.messageStatusRepository
            .findOne({ messageId: id, receiverId: userId });
        if (isRead && !status) {
            const newStatus = new MessageStatus();
            newStatus.messageId = id;
            newStatus.receiverId = userId;
            return this.messageStatusRepository.save(newStatus);
        }
        if (!isRead && status) {
            return this.messageStatusRepository
                .delete({ messageId: id, receiverId: userId })
        }
    }

    findById(id: number): Promise<Message> {
        return this.messagesRepository.findOne(id);
    }

    // ??????
    async getUnreadMessages(userId: User['id']) {
        const userChats = await this.chatsService.getUserChats(userId);

        return Promise.all(userChats.map(async ({ id }) => {
            const count = await this.messagesRepository
                .createQueryBuilder('message')
                .where("message.chatId = :id", { id })
                .andWhere("message.senderID !=:userId", { userId })
                .leftJoinAndSelect(
                    'message.status', 'status',
                )
                .andWhere('status.messageId IS NULL')
                .getCount();
            return { count, chatId: id }
        }))
    }

    async getMessagesByChat(chatId: Chat["id"], userId: User['id'], { limit = 50, page = 0, }: GetMessagesDto
    ) {
        const [rawMessages, total] = await this.messagesRepository
            .createQueryBuilder('message')
            .where("message.chatId = :chatId", { chatId })
            .leftJoinAndSelect('message.replyTo', 'replyTo')
            .leftJoinAndSelect('message.sender', 'sender')
            .leftJoinAndSelect('replyTo.sender', 'replyToSender')
            .leftJoinAndMapOne('message.status', MessageStatus, 'status',
                "message.id=status.messageId AND status.receiverId=:userId",
                { userId })
            .select([
                "message.id", "message.body", "message.createdAt",
                "sender.username", "sender.id",
                "replyTo.id", "replyTo.body", "replyTo.createdAt",
                "replyToSender.username", "replyToSender.id",
                'status.readAt'
            ])
            .orderBy("message.createdAt", "DESC")
            .skip(page * limit)
            .take(limit)
            .getManyAndCount();

        const messages = rawMessages.map(
            ({ status, ...rest }) => ({
                ...rest,
                //@ts-ignore
                read: status?.readAt ?? null
            }))

        return { messages, total }
    }
}
