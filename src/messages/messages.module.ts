import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsModule } from '../chats/chats.module';
import { MessageStatus } from './message-status.entity';
import { MessagesController } from './message.controller';
import { Message } from './message.entity';
import { MessagesService } from './message.service';


@Module({
    imports: [TypeOrmModule.forFeature([Message, MessageStatus]),
    forwardRef(() => ChatsModule)],
    providers: [MessagesService],
    exports: [MessagesService],
    controllers: [MessagesController]
})
export class MessagesModule { }
