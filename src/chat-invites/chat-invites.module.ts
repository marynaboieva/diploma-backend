import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ChatInvitesController } from './chat-invites.controller';
import { ChatInvitesService } from './chat-invites.service';
import { ChatInvite } from './chat-invite.enity';
import { ChatsModule } from '../chats/chats.module';


@Module({
    imports: [
        TypeOrmModule.forFeature([ChatInvite]),
        forwardRef(() => ChatsModule)
    ],
    providers: [ChatInvitesService],
    controllers: [ChatInvitesController],
    exports: [ChatInvitesService],
})
export class ChatInvitesModule { }
