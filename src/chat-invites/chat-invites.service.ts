import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/user.entity';
import { Repository } from 'typeorm';
import { ChatInvite } from './chat-invite.enity';
import { Chat } from '../chats/chat.entity';

@Injectable()
export class ChatInvitesService {
    constructor(
        @InjectRepository(ChatInvite)
        private readonly chatInvitesRepository: Repository<ChatInvite>,
    ) { }

    async createInvite(chatId: Chat['id'], userId: User['id'], expiresAt?: ChatInvite['expiresAt']) {
        const invite = await new ChatInvite();
        invite.chatId = chatId;
        invite.createdById = userId;
        invite.expiresAt = expiresAt;
        return this.chatInvitesRepository.save(invite);
    }

    async findInvite(id: ChatInvite['id']) {
        return this.chatInvitesRepository.findOne({ id });
    }

    async deleteInvite(id: ChatInvite['id']) {
        return this.chatInvitesRepository.delete({ id });
    }
}
