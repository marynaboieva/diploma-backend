import {
    BadRequestException, Body, Controller,
    Delete, ForbiddenException, Get, NotFoundException,
    Param, ParseIntPipe, Post, Put
} from '@nestjs/common';
import { JWTPayload } from 'src/decorators/jwt-payload.decorator';
import { ChatsService } from '../chats/chats.service';
import { ChatInvitesService } from './chat-invites.service';
import { ChatRole } from '../chats/types';

@Controller('invites')
export class ChatInvitesController {
    constructor(
        private readonly chatsService: ChatsService,
        private readonly chatInvitesService: ChatInvitesService,
    ) { }


    @Post(':inviteId/accept')
    async joinChat(
        @JWTPayload() jwtPayload,
        @Param('inviteId') inviteId: string,
    ) {
        const invite = await this.chatInvitesService.findInvite(inviteId);

        if (!invite || !(await this.chatsService.findById(invite.chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        if ((await this.chatsService.getUserInChat(invite.chatId, jwtPayload.id))) {
            throw new BadRequestException('User is already a member of the chat');
        }

        if (invite.expiresAt && invite.expiresAt.getTime() <= Date.now()) {
            throw new BadRequestException("The invitation has expired");
        }

        return await this.chatsService.addMember(invite.chatId, jwtPayload.id);
    }


    @Delete(':inviteId')
    async deleteInvite(
        @JWTPayload() jwtPayload,
        @Param('chatId', ParseIntPipe) chatId: number,
        @Param('inviteId') inviteId: string,
    ) {

        if (!(await this.chatsService.findById(chatId))) {
            throw new NotFoundException("Chat was not found");
        }

        if (!(await this.chatInvitesService.findInvite(inviteId))) {
            throw new NotFoundException("Invite was not found");
        }

        const chatUser = await this.chatsService.getUserInChat(chatId, jwtPayload.id);
        if (chatUser?.role !== ChatRole.ADMIN) {
            throw new ForbiddenException("You do not have permission for this action");
        }

        return await this.chatInvitesService.deleteInvite(inviteId);
    }

}
