import {
    Entity,
    Column,
    CreateDateColumn,
    ManyToOne,
    BeforeInsert,
    PrimaryColumn,

} from 'typeorm';
import { nanoid } from 'nanoid'

import { Chat } from '../chats/chat.entity';
import { User } from '../users/user.entity';


@Entity()
export class ChatInvite {
    @PrimaryColumn({ unique: true })
    id: string;

    @BeforeInsert()
    generateCode() {
        this.id = nanoid(15);
    }

    @Column()
    chatId: number;

    @ManyToOne(() => Chat, chat => chat.invites, { onDelete: 'CASCADE' })
    chat: Chat;

    @Column()
    createdById: number;

    @ManyToOne(() => User, user => user.createdInvites)
    createdBy: User;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @Column({ type: 'timestamp', nullable: true })
    expiresAt: Date;
}
