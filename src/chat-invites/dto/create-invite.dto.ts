import { IsDate, IsOptional } from 'class-validator';

export class CreateInviteDto {
    @IsDate()
    @IsOptional()
    expiresAt: Date;
}
